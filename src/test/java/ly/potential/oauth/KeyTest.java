package ly.potential.oauth;


import com.github.fommil.ssh.SshRsaCrypto;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class KeyTest {

    private static String openSshPrivateKey =
            "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpAIBAAKCAQEAxTID6oqn6+C8/v9QwuNrV83qVmq7aWn1VZ9FteW6Q3XW3Nzx\n" +
            "sDUgEPZua3V6dgHPIPYg2jmVj56HDR1+04PSVmeqmuLBkqhknGEcC7sJ6z6/cwEq\n" +
            "VmSgEUagRQqusUXsk2g4oBM1ljkPwi9KDyj5h2ICsPyEX9WU9bp3/BKIUuFUDy6T\n" +
            "4HI251ttQVwAFxGHbDShYkH5BQVEcr4dGouAPBTEk6WRvV4P6K/llExdGkmfOadv\n" +
            "H4XcAaA+qM4NguFHEYdHKO1jAp+SHuL7QfxpFoebe8Rs4k2ekij13RKEJa6Tgg6k\n" +
            "PFdhgJ8Xj6vrNg5OXgOEmUs4VkgTSdXhCQCwdQIDAQABAoIBAQCPptXPgF6/qKJF\n" +
            "qLyunHlQJ4N0nKAWNMgyLarczZOZHTj/AD1Yvfq0ILFjutsRKFQ5kNNx5c0cKMH9\n" +
            "PeVsNHV/2FkiJ9oX+nAAs4V4zjJ1mupg3xJ2iStKKocKhLOqFGQ5Jg1KH3REESF3\n" +
            "tnde/9x3UWDgibPtFGxFbieGDAIfTRi/ejNPpt32VyHn5TXW0cA3ZZ+CrcA7xD7Q\n" +
            "OGhxxOLcFmXIXkSIm7rt2ft+hsowdl/UJOnmRIIwZuXPj81bl62+w8d0r/YAs1g/\n" +
            "7XJ31Gc/P6/xMdt0UNtZ/SaZynKsHORglq+WXuHySYRFdG6+FJFphwEQC8opkarC\n" +
            "2bn2vozpAoGBAP6uD6bvySqG8cWtaf4KgAjzAmtdps6vD4/a0MavqdVrrI13U66T\n" +
            "SSCwOSesHJ/WC2QGiZ2jqYW+1D0LZ2XLc+7wUp09lpN8XJlS5nRQ8ZBu988EWHWC\n" +
            "yo6YHDx+yQ1bU459coT6yC08/liQdgcLZzQuqbDTIFtzqcdjZrx89QX3AoGBAMY3\n" +
            "rU7X1SYWR8kxb8M/VCU68/6SWnB0MyRT2nWPWQaDL3EEjfXJrfZNBHeTAHO2aTvy\n" +
            "lZZPvRr2Gf4Ki1wJzkNt7hM9gEyet27KIfwpgwygXFZKZDWKfkq4UVXG/lZ24T4Q\n" +
            "OrOWze3PVhzUVLj839Yrvssf6iP4KhYKNTSnBnHzAoGBALCi7LwtpupV+a3O3qqR\n" +
            "gcezM8RfiefjxvlRHGgRQcuCCIV9c46rHJcNYja/yoKZJAFV/3BUKKlYdz93f1k8\n" +
            "yMW57LKeGqdkI507wBaAZWp1bG4qZ2fpNK+9lCfpD2yyILu9CgZtIRUvLoEigJnW\n" +
            "MNgiXRME+3YyPFjL+l9Uk2R1AoGAZ7/2v6DrGS9qTZ3JsPmx0nbkBWiXW+sGZcpa\n" +
            "O1RXtbfumgpXP8JbguLXJrtT7j0ZId6ce3urMLSF8FmWjgs34kVk6r8zK2eb/aph\n" +
            "VXpo9K/vBN+VSgz+g5+sOuiYcatxxCl8gEm76fCvUxG0cssxYgmtqOZAiKk5ZiXb\n" +
            "qX6POg8CgYAFvjouTapwkF0BrucyFcm3M9JkhrSgkad/0FMHu43lkFceFtHfQbuq\n" +
            "ZNNhULVHmI2yy56OM78bXqi5IERWvFbD9+3uRxAVcTa+u1Tf0WWSjs9ZT0lydeQZ\n" +
            "5dDcJlKoJC8TdBaTJ+6NNCbi1JWUHivL7c9AHHwG6NzAf78NmmYRDA==\n" +
            "-----END RSA PRIVATE KEY-----\n";;

    private static String openSshPublicKey =
            "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDFMgPqiqfr4Lz+/1DC42tXzepWartpafVVn0W15bpDddbc3PGwNSAQ9m5rdXp2Ac8g9iDaOZWPnocNHX7Tg9JWZ6qa4sGSqGScYRwLuwnrPr9zASpWZKARRqBFCq6xReyTaDigEzWWOQ/CL0oPKPmHYgKw/IRf1ZT1unf8EohS4VQPLpPgcjbnW21BXAAXEYdsNKFiQfkFBURyvh0ai4A8FMSTpZG9Xg/or+WUTF0aSZ85p28fhdwBoD6ozg2C4UcRh0co7WMCn5Ie4vtB/GkWh5t7xGziTZ6SKPXdEoQlrpOCDqQ8V2GAnxePq+s2Dk5eA4SZSzhWSBNJ1eEJALB1 root@pot-dev-web-01";

    private SshRsaCrypto rsa = new SshRsaCrypto();

    @Test
    public void tryPrivate() throws GeneralSecurityException, IOException {

        PrivateKey privateKey = rsa.readPrivateKey(rsa.slurpPrivateKey(openSshPrivateKey));

        assertThat(privateKey.getAlgorithm(), is("RSA"));
        assertThat(privateKey.getFormat(), is("PKCS#8"));

    }

    @Test
    public void tryPublic() throws IOException, GeneralSecurityException {

        PublicKey publicKey = rsa.readPublicKey(rsa.slurpPublicKey(openSshPublicKey));

        assertThat(publicKey.getAlgorithm(), is("RSA"));
        assertThat(publicKey.getFormat(), is("X.509"));
    }

    public String testFile(String filename) throws IOException {
        ClassPathResource r = new ClassPathResource(filename);
        return IOUtils.toString(r.getInputStream());
    }
}
