package ly.potential.oauth.tokenstore;

import ly.potential.oauth.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

@ActiveProfiles("integration-test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes={Application.class})
@WebAppConfiguration
@IntegrationTest
public class MongoTokenStoreTest {

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    @Autowired
    private MongoTokenStore mongoTokenStore;

    private Map<String, Object> testData = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        accessTokenRepository.deleteAll();
    }

    @Test
    public void shouldStoreAnAccessTokenWithoutBarfing() {

        aStoredAccessToken();
    }

    @Test
    public void shouldRetrieveAStoredAccessTokenById() {

        // given
        aStoredAccessToken();

        // when
        OAuth2AccessToken accessToken = mongoTokenStore.readAccessToken("token-id");

        // then
        assertThat(accessToken, is(notNullValue()));
    }

    private void aStoredAccessToken() {
        OAuth2AccessToken accessToken = accessToken();

        OAuth2Authentication authentication = authentication();

        mongoTokenStore.storeAccessToken(accessToken, authentication);
    }

    private OAuth2AccessToken accessToken() {
        return (OAuth2AccessToken)testData.computeIfAbsent("accessToken", key -> new DefaultOAuth2AccessToken("token-id"));
    }

    private OAuth2Request request() {
        return (OAuth2Request)testData.computeIfAbsent("request", key -> new OAuth2Request(emptyMap(), "client-id", emptyList(), true, emptySet(), emptySet(), null, emptySet(), emptyMap()));
    }

    private OAuth2Authentication authentication() {
        return (OAuth2Authentication)testData.computeIfAbsent("authentication", key -> new OAuth2Authentication(request(), new UsernamePasswordAuthenticationToken("user", "pass")));
    }


}