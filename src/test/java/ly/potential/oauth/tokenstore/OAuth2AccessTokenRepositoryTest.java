package ly.potential.oauth.tokenstore;

import ly.potential.oauth.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@ActiveProfiles("integration-test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes={Application.class})
@WebAppConfiguration
@IntegrationTest
public class OAuth2AccessTokenRepositoryTest {

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;


    @Test
    public void shouldAlsoRead() {
        accessTokenRepository.findOne("foo");
        accessTokenRepository.findByTokenId("foo");
        accessTokenRepository.findByAuthenticationId("foo");
        accessTokenRepository.findByClientId("foo");
        accessTokenRepository.findByClientIdAndUserName("foo", "bar");
        accessTokenRepository.findByRefreshToken("foo");
    }

    @Test
    public void shouldAlsoRead2() {

        refreshTokenRepository.findByTokenId("foo");
    }


}