package ly.potential.oauth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import ly.potential.oauth.tokenstore.AccessTokenRepository;
import ly.potential.oauth.tokenstore.RefreshTokenRepository;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.*;

@ActiveProfiles("integration-test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes={Application.class})
@WebAppConfiguration
@IntegrationTest
public class ApplicationTest {

    public static final Condition<Throwable> status(HttpStatus status) {
        return new Condition<>(
                e -> ((HttpClientErrorException) e).getStatusCode().equals(status),
                "Expected status " + status);
    }

    @Resource
    private RestTemplate restTemplate;
    @Resource
    private AccessTokenRepository accessTokenRepository;
    @Resource
    private RefreshTokenRepository refreshTokenRepository;

    @Before
    public void setUp() throws Exception {
        accessTokenRepository.deleteAll();
        refreshTokenRepository.deleteAll();
    }

    @Test
    public void testUnprotectedResource() {

        ResponseEntity<String> response = getFreeResource();

        assertThat(response.getStatusCode(), is(OK));
        assertThat(response.getBody(), is("Hello, world"));
    }

    @Test
    public void testProtectedResourceWithoutToken() {

        assertThatThrownBy(this::getProtectedResourceWithoutToken)
                .isInstanceOf(HttpClientErrorException.class)
                .has(status(UNAUTHORIZED));
    }

    @Test
    public void testResourceOwnerGrantWithBadToken() {

        assertThatThrownBy(this::getProtectedResourceWithBadToken)
                .isInstanceOf(HttpClientErrorException.class)
                .has(status(UNAUTHORIZED));
    }

    @Test
    public void shouldNotGiveAGrantForBadResourceOwnerCredentials() {

        assertThatThrownBy(this::getPasswordGrantBadCredentials)
                .isInstanceOf(HttpClientErrorException.class)
                .has(status(BAD_REQUEST));
    }

    @Test
    public void testResourceOwnerPasswordGrant() {

        AuthToken token = getPasswordGrant();

        ResponseEntity<String> response = getProtectedResponse(token);

        assertThat(response.getStatusCode(), is(OK));
        assertThat(response.getBody(), is("Hello, min"));
    }

    @Test
    public void testRefreshTokenGrant() {

        AuthToken initialToken = getPasswordGrant();

        AuthToken token = getRefreshedToken(initialToken);

        ResponseEntity<String> response = getProtectedResponse(token);

        assertThat(response.getStatusCode(), is(OK));
        assertThat(response.getBody(), is("Hello, min"));

    }

    private AuthToken getRefreshedToken(AuthToken initialToken) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic cG90ZW50aWFsbHktd2Vic2l0ZTpNSGgwY1VWZTkw");
        HttpEntity<String> request = new HttpEntity<>("", headers);

        ResponseEntity<AuthToken> authResponse = restTemplate.exchange(
                "http://localhost:8080/oauth/token?grant_type=refresh_token&refresh_token={refresh_token}",
                POST, request, AuthToken.class, initialToken.refreshToken);

        return authResponse.getBody();
    }

    private ResponseEntity<String> getProtectedResourceWithBadToken() {
        return getProtectedResponse(new AuthToken("foo", "", "", 0, ""));
    }

    private ResponseEntity<String> getProtectedResourceWithoutToken() {
        return restTemplate.getForEntity("http://localhost:8080/protected-resource", String.class);
    }

    private ResponseEntity<String> getProtectedResponse(AuthToken token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + token.accessToken);
        HttpEntity<String> request = new HttpEntity<>("", headers);

        return restTemplate.exchange("http://localhost:8080/protected-resource", GET, request, String.class);
    }

    private ResponseEntity<String> getFreeResource() {

        return restTemplate.getForEntity("http://localhost:8080/free-resource", String.class);
    }

    private AuthToken getPasswordGrant() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic cG90ZW50aWFsbHktd2Vic2l0ZTpNSGgwY1VWZTkw");
        HttpEntity<String> request = new HttpEntity<>("", headers);

        ResponseEntity<AuthToken> authResponse = restTemplate.exchange(
                "http://localhost:8080/oauth/token?grant_type=password&username=min&password=min&scope=microservices",
                POST, request, AuthToken.class);

        //ResponseEntity<String> authStringResponse = restTemplate.exchange(
        //        "http://localhost:8080/oauth/token?grant_type=password&username=min&password=min&scope=microservices",
        //        POST, request, String.class);

        return authResponse.getBody();
    }

    private ResponseEntity<AuthToken> getPasswordGrantBadCredentials() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic cG90ZW50aWFsbHktd2Vic2l0ZTpNSGgwY1VWZTkw");
        HttpEntity<String> request = new HttpEntity<>("", headers);

        return restTemplate.exchange(
                "http://localhost:8080/oauth/token?grant_type=password&username=min&password=wrong&scope=microservices",
                POST, request, AuthToken.class);
    }

    static class AuthToken {
        private String accessToken;
        private String tokenType;
        private String refreshToken;
        private long expiresIn;
        private String scope;

        @JsonCreator
        public AuthToken(@JsonProperty("access_token") String accessToken,
                         @JsonProperty("token_type") String tokenType,
                         @JsonProperty("refresh_token") String refreshToken,
                         @JsonProperty("expires_in") long expiresIn,
                         @JsonProperty("scope") String scope) {
            this.accessToken = accessToken;
            this.tokenType = tokenType;
            this.refreshToken = refreshToken;
            this.expiresIn = expiresIn;
            this.scope = scope;
        }
    }

}
