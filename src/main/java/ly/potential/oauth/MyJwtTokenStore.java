package ly.potential.oauth;

import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import static java.lang.System.out;

/**
 * Created by jtownson on 21/07/2015.
 */
public class MyJwtTokenStore extends JwtTokenStore {

    public MyJwtTokenStore(JwtAccessTokenConverter jwtAccessTokenConverter) {
        super(jwtAccessTokenConverter);
    }

    @Override
    public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
        out.println("readAuthentication(OAuth2AccessToken token)");
        return super.readAuthentication(token);
    }

    @Override
    public OAuth2Authentication readAuthentication(String token) {
        out.println("readAuthentication(String token)");
        return super.readAuthentication(token);
    }

    @Override
    public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
        out.println("storeAccessToken");
        super.storeAccessToken(token, authentication);
    }

    @Override
    public OAuth2AccessToken readAccessToken(String tokenValue) {
        out.println("readAccessToken");
        return super.readAccessToken(tokenValue);
    }

    @Override
    public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
        out.println("storeRefreshToken");
        super.storeRefreshToken(refreshToken, authentication);
    }

    @Override
    public OAuth2RefreshToken readRefreshToken(String tokenValue) {
        out.println("readRefreshToken");
        return super.readRefreshToken(tokenValue);
    }

    @Override
    public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
        out.println("readAuthenticationForRefreshToken");
        return super.readAuthenticationForRefreshToken(token);
    }

    @Override
    public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
        out.println("getAccessToken");
        return super.getAccessToken(authentication);
    }
}
