package ly.potential.oauth;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

class UserAuthenticationManager implements AuthenticationManager {

    private UserAuthenticationProvider apiUserAuthenticationProvider;

    public UserAuthenticationManager(UserAuthenticationProvider apiUserAuthenticationProvider) {
        this.apiUserAuthenticationProvider = apiUserAuthenticationProvider;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return apiUserAuthenticationProvider.authenticate(authentication);
    }
}
