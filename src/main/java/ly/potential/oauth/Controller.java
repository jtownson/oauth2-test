package ly.potential.oauth;

import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @RequestMapping("/protected-resource")
    public String home(@AuthenticationPrincipal String user) {

        return "Hello, " + user;
    }

    @RequestMapping("/free-resource")
    public String home() {

        return "Hello, world";
    }

}
