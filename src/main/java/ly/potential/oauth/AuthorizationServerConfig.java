package ly.potential.oauth;

import ly.potential.oauth.tokenstore.MongoTokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import sun.security.provider.DSAPublicKeyImpl;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import static java.util.Arrays.asList;

/**
 * Created by jtownson on 21/07/2015.
 */
@Configuration
@EnableAuthorizationServer
class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    //@Autowired
    //private TokenSer
    //@Autowired
    //private MongoTokenStore tokenStore;

    //@Autowired
    //private JwtTokenStore


    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {

        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();

        PublicKey publicKey = null;//new DSAPublicKeyImpl()
        PrivateKey privateKey = null;
        converter.setKeyPair(new KeyPair(publicKey, privateKey));

        converter.setSigningKey("5DYye3IY9nQ3e9oU940wgvO4W8Rt0XA6");
        converter.afterPropertiesSet();
        //converter.setVerifierKey("77DTPZincr5PCXfZ4LEw89cGt0kNAQwB");

        JwtTokenStore tokenStore = new MyJwtTokenStore(converter);

        UserDetailsService userDetailsService = userName -> {
            if ("min".equals(userName)) {
                return new User("min", "min", asList());
            }
            return null;
        };

        UserAuthenticationProvider userAuthenticationProvider = new UserAuthenticationProvider(userDetailsService);

        UserAuthenticationManager userAuthenticationManager = new UserAuthenticationManager(userAuthenticationProvider);

        endpoints.tokenEnhancer(converter).authenticationManager(userAuthenticationManager).userDetailsService(userDetailsService).tokenStore(tokenStore);
    }

    @Override
    public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {

        // TODO do apiusers lookup here.
        clients.withClientDetails(clientId -> {

            BaseClientDetails clientDetails = new BaseClientDetails();
            clientDetails.setClientId("potentially-website");
            clientDetails.setClientSecret("MHh0cUVe90");
            clientDetails.setScope(asList("microservices"));
            clientDetails.setAuthorizedGrantTypes(asList("password", "refresh_token"));

            return clientDetails;
        });
    }

}
