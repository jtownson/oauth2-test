package ly.potential.oauth;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import static java.util.Collections.EMPTY_LIST;

class UserAuthenticationProvider implements AuthenticationProvider {

    private UserDetailsService userDetailsService;

    public UserAuthenticationProvider(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        if (authentication.getCredentials() == null) {
            throw new BadCredentialsException("No credentials provided to authentication");
        }

        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        if (isAuthentic(name, password)) {
            return new UsernamePasswordAuthenticationToken(name, password, EMPTY_LIST);
        } else {
            throw new BadCredentialsException("Unable to authenticate user " + name);
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.equals(authentication);
    }

    private boolean isAuthentic(String name, String password) {

        if (StringUtils.isBlank(name)) {
            return false;
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(name);

        if (userDetails == null) {
            return false;
        }

        return name.equalsIgnoreCase(userDetails.getUsername()) && password.equals(userDetails.getPassword());
    }
}
