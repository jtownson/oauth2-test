package ly.potential.oauth;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import ly.potential.oauth.tokenstore.AccessTokenRepository;
import ly.potential.oauth.tokenstore.PreAuthenticatedReadConverter;
import ly.potential.oauth.tokenstore.RefreshTokenRepository;
import ly.potential.oauth.tokenstore.UsernamePasswordReadConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import static java.util.Arrays.asList;

@Configuration
@PropertySource("mongo-oauth2-config.properties")
@EnableMongoRepositories(basePackageClasses = {AccessTokenRepository.class, RefreshTokenRepository.class})
public class MongoConfig extends AbstractMongoConfiguration {

    @Value("${database.server}")
    private String databaseServer;

    @Value("${database.name}")
    private String databaseName;

    @Value("${database.user}")
    private String databaseUser;

    @Value("${database.password}")
    private String databasePassword;

    @Override
    public @Bean MongoDbFactory mongoDbFactory() throws Exception {

        Mongo mongoClient = new MongoClient(new MongoClientURI(databaseServer));

        UserCredentials userCredentials = new UserCredentials(databaseUser, databasePassword);

        return new SimpleMongoDbFactory(mongoClient, databaseName, userCredentials);
    }

    @Override
    protected String getDatabaseName() {
        return databaseName;
    }


    @Override
    @Bean
    public Mongo mongo() throws Exception {
        return new MongoClient(new MongoClientURI(databaseServer));
    }

    @Bean
    @Override
    public CustomConversions customConversions() {

        return new CustomConversions(asList(
                new UsernamePasswordReadConverter(),
                new PreAuthenticatedReadConverter()));
    }

}