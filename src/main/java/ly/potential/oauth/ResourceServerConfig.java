package ly.potential.oauth;

import ly.potential.oauth.tokenstore.MongoTokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;


@Configuration
@EnableResourceServer
class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    //@Autowired
    //private MongoTokenStore tokenStore;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/protected-resource/**").access("#oauth2.hasScope('microservices')");
        http.authorizeRequests().antMatchers("/free-resource/**").permitAll();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey("5DYye3IY9nQ3e9oU940wgvO4W8Rt0XA6");
        converter.afterPropertiesSet();
        //converter.setVerifierKey("77DTPZincr5PCXfZ4LEw89cGt0kNAQwB");
        JwtTokenStore tokenStore = new MyJwtTokenStore(converter);
        resources.tokenStore(tokenStore).resourceId("foo");
    }
}
