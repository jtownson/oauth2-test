package ly.potential.oauth.tokenstore;

import com.mongodb.DBObject;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import static java.util.Arrays.asList;

public class UsernamePasswordReadConverter implements Converter<DBObject, UsernamePasswordAuthenticationToken> {
    @Override
    public UsernamePasswordAuthenticationToken convert(DBObject source) {
        /*
            "userAuthentication" : {
            "_class" : "org.springframework.security.authentication.UsernamePasswordAuthenticationToken",
            "principal" : "user",
            "credentials" : "pass",
            "authorities" : [],
            "authenticated" : false
        },
         */
        String principal = (String)source.get("principal");
        String credentials = (String)source.get("credentials");

        return new UsernamePasswordAuthenticationToken(principal, credentials, asList());
    }
}
