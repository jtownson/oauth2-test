package ly.potential.oauth.tokenstore;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

public class AuthenticationAccessToken {

    @Id
    private String tokenId;

    @Indexed
    private String refreshToken;

    @Indexed
    private String authenticationId;

    @Indexed
    private String userName;

    @Indexed
    private String clientId;

    private OAuth2AccessToken accessToken;

    private OAuth2Authentication authentication;

    @JsonCreator
    public AuthenticationAccessToken(@JsonProperty("tokenId") String tokenId,
                                     @JsonProperty("refreshToken") String refreshToken,
                                     @JsonProperty("authenticationId") String authenticationId,
                                     @JsonProperty("userName") String userName,
                                     @JsonProperty("clientId") String clientId,
                                     @JsonProperty("accessToken") OAuth2AccessToken accessToken,
                                     @JsonProperty("authentication") OAuth2Authentication authentication) {
        this.tokenId = tokenId;
        this.refreshToken = refreshToken;
        this.authenticationId = authenticationId;
        this.userName = userName;
        this.clientId = clientId;
        this.accessToken = accessToken;
        this.authentication = authentication;
    }

    public String getTokenId() {
        return tokenId;
    }

    public OAuth2AccessToken getAccessToken() {
        return accessToken;
    }

    public OAuth2Authentication getAuthentication() {
        return authentication;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getAuthenticationId() {
        return authenticationId;
    }

    public String getUserName() {
        return userName;
    }

    public String getClientId() {
        return clientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AuthenticationAccessToken that = (AuthenticationAccessToken) o;

        return new EqualsBuilder()
                .append(tokenId, that.tokenId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(tokenId)
                .toHashCode();
    }
}
