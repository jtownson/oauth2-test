package ly.potential.oauth.tokenstore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.DefaultAuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MongoTokenStore implements TokenStore {

    private final AccessTokenRepository accessTokenRepository;

    private final RefreshTokenRepository refreshTokenRepository;

    private AuthenticationKeyGenerator authenticationKeyGenerator = new DefaultAuthenticationKeyGenerator();

    @Autowired
    public MongoTokenStore(AccessTokenRepository accessTokenRepository,
                           RefreshTokenRepository refreshTokenRepository) {
        this.accessTokenRepository = accessTokenRepository;
        this.refreshTokenRepository = refreshTokenRepository;
    }

    @Override
    public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
        return readAuthentication(token.getValue());
    }

    @Override
    public OAuth2Authentication readAuthentication(String tokenId) {
        return accessTokenRepository.findByTokenId(tokenId).getAuthentication();
    }

    @Override
    public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {

        AuthenticationAccessToken authenticationAccessToken = new AuthenticationAccessToken(
                token.getValue(),
                token.getRefreshToken() == null ? null : token.getRefreshToken().getValue(),
                authenticationKeyGenerator.extractKey(authentication),
                String.valueOf(authentication.getPrincipal()),
                authentication.getOAuth2Request().getClientId(),
                token, authentication);

        accessTokenRepository.save(authenticationAccessToken);
    }

    @Override
    public OAuth2AccessToken readAccessToken(String tokenValue) {

        AuthenticationAccessToken token = accessTokenRepository.findByTokenId(tokenValue);
        if (token == null) {
            return null;
        }
        return token.getAccessToken();

    }

    @Override
    public void removeAccessToken(OAuth2AccessToken token) {
        AuthenticationAccessToken accessToken = accessTokenRepository.findByTokenId(token.getValue());
        if (accessToken != null) {
            accessTokenRepository.delete(accessToken);
        }
    }

    @Override
    public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
        AuthenticationRefreshToken authenticationRefreshToken =
                new AuthenticationRefreshToken(refreshToken.getValue(), refreshToken, authentication);
        refreshTokenRepository.save(authenticationRefreshToken);
    }

    @Override
    public OAuth2RefreshToken readRefreshToken(String tokenValue) {
        return refreshTokenRepository.findByTokenId(tokenValue).getRefreshToken();
    }

    @Override
    public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
        return refreshTokenRepository.findByTokenId(token.getValue()).getAuthentication();
    }

    @Override
    public void removeRefreshToken(OAuth2RefreshToken token) {
        refreshTokenRepository.delete(refreshTokenRepository.findByTokenId(token.getValue()));
    }

    @Override
    public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
        accessTokenRepository.delete(accessTokenRepository.findByRefreshToken(refreshToken.getValue()));
    }

    @Override
    public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
        AuthenticationAccessToken token =  accessTokenRepository.findByAuthenticationId(authenticationKeyGenerator.extractKey(authentication));
        return token == null ? null : token.getAccessToken();
    }

    @Override
    public Collection<OAuth2AccessToken> findTokensByClientId(String clientId) {
        List<AuthenticationAccessToken> tokens = accessTokenRepository.findByClientId(clientId);
        return extractAccessTokens(tokens);
    }

    @Override
    public Collection<OAuth2AccessToken> findTokensByClientIdAndUserName(String clientId, String userName) {
        List<AuthenticationAccessToken> tokens = accessTokenRepository.findByClientIdAndUserName(clientId, userName);
        return extractAccessTokens(tokens);
    }

    private Collection<OAuth2AccessToken> extractAccessTokens(List<AuthenticationAccessToken> tokens) {
        return tokens.stream().map(AuthenticationAccessToken::getAccessToken).collect(Collectors.toList());
    }

}
