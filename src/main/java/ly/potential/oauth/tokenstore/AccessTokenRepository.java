package ly.potential.oauth.tokenstore;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AccessTokenRepository extends MongoRepository<AuthenticationAccessToken, String> {

    AuthenticationAccessToken findByTokenId(String tokenId);

    AuthenticationAccessToken findByRefreshToken(String refreshToken);

    AuthenticationAccessToken findByAuthenticationId(String authenticationId);

    List<AuthenticationAccessToken> findByClientIdAndUserName(String clientId, String userName);

    List<AuthenticationAccessToken> findByClientId(String clientId);
}
