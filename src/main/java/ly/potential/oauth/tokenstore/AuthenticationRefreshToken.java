package ly.potential.oauth.tokenstore;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

public class AuthenticationRefreshToken {

    @Id
    private final String tokenId;
    private final OAuth2RefreshToken refreshToken;
    private final OAuth2Authentication authentication;

    @JsonCreator
    public AuthenticationRefreshToken(@JsonProperty("tokenId") String tokenId,
                                      @JsonProperty("refreshToken") OAuth2RefreshToken refreshToken,
                                      @JsonProperty("authentication") OAuth2Authentication authentication) {
        this.tokenId = tokenId;
        this.refreshToken = refreshToken;
        this.authentication = authentication;
    }

    public String getTokenId() {
        return tokenId;
    }

    public OAuth2RefreshToken getRefreshToken() {
        return refreshToken;
    }

    public OAuth2Authentication getAuthentication() {
        return authentication;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AuthenticationRefreshToken that = (AuthenticationRefreshToken) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(tokenId, that.tokenId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(tokenId)
                .toHashCode();
    }
}
