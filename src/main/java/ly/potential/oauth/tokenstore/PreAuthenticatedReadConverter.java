package ly.potential.oauth.tokenstore;

import com.mongodb.DBObject;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import static java.util.Arrays.asList;


public class PreAuthenticatedReadConverter implements Converter<DBObject, PreAuthenticatedAuthenticationToken> {

    @Override
    public PreAuthenticatedAuthenticationToken convert(DBObject source) {

        /*
            "userAuthentication" : {
            "_class" : "org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken",
            "principal" : {
                "_class" : "org.springframework.security.core.userdetails.User",
                "username" : "min",
                "authorities" : [],
                "accountNonExpired" : true,
                "accountNonLocked" : true,
                "credentialsNonExpired" : true,
                "enabled" : true
            },
            "credentials" : "",
            "authorities" : [],
            "authenticated" : true
        },
         */

        String principal = (String)((DBObject)source.get("principal")).get("username");
        String credentials = (String)source.get("credentials");

        return new PreAuthenticatedAuthenticationToken(principal, credentials, asList());
    }

}
