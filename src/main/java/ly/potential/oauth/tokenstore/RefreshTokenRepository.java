package ly.potential.oauth.tokenstore;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface RefreshTokenRepository extends MongoRepository<AuthenticationRefreshToken, String> {

    AuthenticationRefreshToken findByTokenId(String tokenId);
}
